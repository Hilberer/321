import express from 'express'
import helmet from 'helmet'
import morgan from 'morgan'
import cors from 'cors'
import Configuration from './configurations/Configuration.js'
import Middlewares from './middlewares/Middlewares.js'
import UserRoutes from './routes/User.routes.js'

const application = express()
application.use(cors({ credentials: true}))
application.use(express.json())
application.use(helmet())
application.use(morgan('common'))

UserRoutes.routes(application)
application.use(Middlewares.notFound)
application.use(Middlewares.errorHandler)

Configuration.connectToDatabase()
Configuration.connectToPort(application)

export default application