import UserModel from '../models/User.model.js'
import StatusCode from '../configurations/StatusCode.js'
const createUser = async (request, response) => {

    const user = new UserModel({
        username: request.body.username,
        password: request.body.password
    })


    try {
        const databaseResponse = await user.save()
        response.status(StatusCode.CREATED).send(databaseResponse)

    } catch (error) {
        response.status(StatusCode.INTERNAL_SERVER_ERROR).send({
            message: 'Error while trying to create user',
            stack: error
        })
    }
}

export default {
    createUser
}