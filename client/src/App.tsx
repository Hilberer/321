import * as React from "react"
import {
  ChakraProvider,
  Box,
  Grid,
  theme,
} from "@chakra-ui/react"
import { ColorModeSwitcher } from "./ColorModeSwitcher"
import { Routes } from './routes/Routes'
import { Navigation } from './components/Navigation'

export const App = () => (
  <ChakraProvider theme={theme}>
    <Routes>
      <Navigation />
    </Routes>
      <Grid minH="100vh" p={3}>
        <ColorModeSwitcher justifySelf="flex-end" />
      </Grid>
  </ChakraProvider>
)
