import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import RoutingPath from '../routes/RoutingPath'
import {
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
  } from "@chakra-ui/react"

export const Navigation = () => {
    return (
        <Breadcrumb>
            <BreadcrumbItem>
                <BreadcrumbLink as={RouterLink} to={RoutingPath.home} >
                    Home
                </BreadcrumbLink>
            </BreadcrumbItem>
            <BreadcrumbItem>
                <BreadcrumbLink as={RouterLink} to={RoutingPath.about}>
                    About
                </BreadcrumbLink>
            </BreadcrumbItem>

        </Breadcrumb>
    )
}
